import { Module, VuexModule, Mutation } from 'vuex-module-decorators';



@Module({ name: 'ApplicationModule' })
export default class GraduationModule extends VuexModule {
  public drawer = false;

  @Mutation
  public toggleDrawer() {
    this.drawer = !this.drawer;
  }
}
