psql << EOF
    CREATE USER "abitura" WITH PASSWORD 'abitura';
    ALTER ROLE "abitura" SUPERUSER;
    CREATE DATABASE "abitura";
    GRANT ALL PRIVILEGES ON DATABASE "abitura" to "abitura";
EOF
