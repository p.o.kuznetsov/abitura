from django.apps import apps
from django.contrib import admin
from django import forms

from server.apps.abitura import models


class SubjectScoreForm(forms.ModelForm):
    attempt = forms.ModelChoiceField(queryset=models.Attempt.objects.all(), required=False)
    direction_relation = forms.ModelChoiceField(queryset=models.ProgramDirectionRelation.objects.all(), required=False)


for model in apps.get_models():
    if models.SubjectScore._meta.model == model:
        class SubjectScoreAdmin(admin.ModelAdmin):
            form = SubjectScoreForm

        admin.site.register(model, SubjectScoreAdmin)
        continue
    if model._meta.app_label == 'abitura':
        admin.site.register(model, admin.ModelAdmin)
