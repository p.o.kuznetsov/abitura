import { IRateParams, IProgramParams } from './../../interfaces';
import { Module, VuexModule, MutationAction } from 'vuex-module-decorators';
import { IProgramInstance, IAbiturientRate } from '@/interfaces';
import { getAbiturientRates, getProgram, getPrograms } from '@/api';


@Module({ name: 'ProgramModule' })
export default class ProgramModule extends VuexModule {
  public rates: IAbiturientRate[] = [];

  public program: IProgramInstance = {
    id: 1,
    durationYears: 4,
    educationForm: {
      code: '1',
      name: 'Очное',
    },
    educationProgram: {
      code: '09.03.01',
      name: 'ИВТ',
    },
    graduation: {
      code: '1',
      name: 'Бакалавриат',
    },
    subjects: [],
    career: [],
    directions: [],
  };

  public programs: IProgramInstance[] = [
    this.program,
  ];

  @MutationAction({ mutate: ['programs'] })
  public async fetchPrograms(params: IProgramParams) {
    const programs = await getPrograms(params.educationForm, params.graduation);
    return { programs };
  }

  @MutationAction({ mutate: ['program'] })
  public async fetchProgram(id: number) {
    const program = await getProgram(id);
    return { program };
  }

  @MutationAction({ mutate: ['rates'] })
  public async fetchRate(params: IRateParams) {
    const rates = await getAbiturientRates(params.programInstanceId, params.year, params.direction);
    return { rates };
  }
}
