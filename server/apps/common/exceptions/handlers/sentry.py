# built-in
import logging

# django
from django.db import OperationalError
from rest_framework import status, response as django_response

# app
from server.apps.common.exceptions.handlers.response import get_response

logger = logging.getLogger('django')


def sentry_patch_request_exception_handler(exc, context):
    """Exception handler for sentry."""
    if isinstance(exc, OperationalError):
        logger.error('Got supressed exception %s, returning 503', repr(exc))
        return django_response.Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)

    context_request = context['request']
    if context_request.method.upper() in {'POST', 'PUT', 'PATCH'}:
        context_request._request.POST = context_request.data  # noqa: WPS437
        # have to store it as per django`s request.POST is wiped out on every read attempt
        context_request._request._PERSISTENT_REQUEST_DATA = context_request.data  # noqa: WPS437

    return get_response(exc, context)  # noqa: WPS331
