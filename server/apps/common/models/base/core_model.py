# django
from django.db import models


class ORMCoreModel(models.Model):
    """Base model for django."""

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
