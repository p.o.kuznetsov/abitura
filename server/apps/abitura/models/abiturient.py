from django.contrib.auth.backends import UserModel
from django.db import models

from server.apps.common.models.base import ORMCoreModel


class Abiturient(UserModel):
    middle_name = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return (
            f'id={self.id}, last_name={self.last_name}, ' +
            f'first_name={self.first_name}, middle_name={self.middle_name}'
        )


class Attempt(ORMCoreModel):
    abiturient = models.ForeignKey('Abiturient', on_delete=models.CASCADE, related_name='attempts')
    direction = models.ForeignKey('ProgramDirectionRelation', on_delete=models.CASCADE, related_name='attempts')
    has_accepted = models.BooleanField(default=False)
    has_original_document = models.BooleanField(default=False)

    def __str__(self):
        return (
            f'id={self.id}, abiturient={self.abiturient} ' +
            f'direction={self.direction}, has_accepted={self.has_accepted}, ' +
            f'has_original_document={self.has_original_document}'
        )
