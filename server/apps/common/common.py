# built-in
from typing import List


class ErrorFormatter:  # noqa: WPS306
    """Класс используется для форматирования ошибок."""

    @staticmethod
    def format_error(errors: List[str], status_code: int = 500):
        """Форматирует ошибки в стандартный вид."""
        return {'errors': errors, 'status_code': status_code, 'is_error': True}
