import { IAbiturientRate, IEducationForm, IProgramInstance } from '@/interfaces';
import axios, { AxiosRequestConfig } from 'axios';

const baseURL = process.env.VUE_APP_BASE_URL;

const client = axios.create({
  baseURL,
});

export async function getPrograms(educationForm: string, graduation: string): Promise<IProgramInstance[]> {
  const config = {
    params: { education_form: educationForm, graduation: graduation },
  } as AxiosRequestConfig;
  const response = await client.get('/education-instances', config);
  return response.data as IProgramInstance[];
}

export async function getProgram(id: number): Promise<IProgramInstance> {
  const response = await client.get(`/education-instances/${id}`);
  return response.data as IProgramInstance;
}

export async function getAbiturientRates(program_instance_id: number, year: number, direction: String): Promise<IAbiturientRate[]> {
  const config = {
    params: { program_instance_id, year, direction },
  } as AxiosRequestConfig;
  const response = await client.get(`/abiturient-rates`, config);
  return response.data as IAbiturientRate[];
}

export async function getEducationForms(): Promise<IEducationForm[]> {
  const response = await client.get(`/education-forms`);
  return response.data as IEducationForm[];
}
