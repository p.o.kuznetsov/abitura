from django_filters import rest_framework as filters

from server.apps.abitura import models


class EducationProgramInstanceFilter(filters.FilterSet):
    education_form = filters.CharFilter(lookup_expr='code')
    graduation = filters.CharFilter(lookup_expr='code')

    class Meta:
        model = models.EducationProgramInstance
        fields = ('education_form', 'graduation')


class AttemptRateFilter(filters.FilterSet):
    program_instance_id = filters.NumberFilter(
        'direction__program_instance',
        lookup_expr='id',
        required=True,
    )
    year = filters.NumberFilter(
        'direction',
        lookup_expr='year',
        required=True,
    )
    direction = filters.CharFilter(
        'direction__program_direction',
        lookup_expr='code',
    )

    class Meta:
        model = models.Attempt
        fields = ('program_instance_id', 'year')
