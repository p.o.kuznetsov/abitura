# django
from django.core.management.base import BaseCommand
from django.core.management import call_command


class Command(BaseCommand):
    help = 'Fill database.'

    def handle(self, *args, **options):
        call_command(
            'loaddata',
            'server/apps/abitura/fixtures/data.json',
        )
