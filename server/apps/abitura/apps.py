# django
from django.apps import AppConfig


class AbituraConfig(AppConfig):
    """Config for dms."""

    name = 'server.apps.abitura'
