# django
from django.conf.urls import url
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny
from rest_framework.routers import SimpleRouter

# app
from server.apps.abitura.urls import app_name
from server.apps.api.v1 import views
from . import version

router = SimpleRouter(trailing_slash=False)
router.register('graduations', views.GraduationViewSet)
router.register('education-programs', views.EducationProgramViewSet)
router.register('education-forms', views.EducationFormViewSet)
router.register('education-instances', views.EducationProgramInstanceViewSet)
router.register('abiturient-rates', views.AbiturientRateViewSet)

urls = [
    url('', include((router.urls, app_name), namespace=version)),
]
doc_schema = get_schema_view(
    openapi.Info(
        title='Portfolio api',
        default_version=version,
    ),
    patterns=[
        url(f'api/{version}/', include(urls)),  # noqa: DJ05 - namespace for swagger isn' required.
    ],
    public=True,
    permission_classes=(AllowAny,),
)

urlpatterns = [
    path('documentation', doc_schema.with_ui('swagger', cache_timeout=0), name=f'swagger-{version}'),
] + urls
