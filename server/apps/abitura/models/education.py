from django.db import models

from server.apps.common.models.base import ORMCoreModel
from .mixins import CodeMixin, YearMixin


class Graduation(CodeMixin):
    """Бакалавриат, магистратура."""


class EducationForm(CodeMixin):
    """Очное, заочное"""


class EducationProgram(CodeMixin):
    """09.03.01"""


class ProgramDirection(CodeMixin):
    """Целевое, контрактное."""


class ProgramDirectionRelation(YearMixin):
    """Целевое по информатике на 2013 год."""

    program_direction = models.ForeignKey(
        'ProgramDirection',
        on_delete=models.CASCADE,
        related_name='direction_relations',
    )
    program_instance = models.ForeignKey(
        'EducationProgramInstance',
        on_delete=models.CASCADE,
        related_name='direction_relations',
    )
    places_count = models.SmallIntegerField(default=0)


class EducationProgramInstance(ORMCoreModel):
    """Информатика для заочников-бакалавров."""

    education_form = models.ForeignKey('EducationForm', on_delete=models.CASCADE, related_name='education_programs')
    education_program = models.ForeignKey(
        'EducationProgram',
        on_delete=models.CASCADE,
        related_name='program_instances',
    )
    graduation = models.ForeignKey('Graduation', on_delete=models.CASCADE, related_name='program_instances')
    duration_years = models.PositiveSmallIntegerField(default=4)
    program_directions = models.ManyToManyField(
        'ProgramDirection',
        related_name='program_instances',
        through='ProgramDirectionRelation',
    )

    def __str__(self):
        return f'id={self.id}, duration_years={self.duration_years}, education_form={self.education_form}'
