# built-in
from typing import Union

# django
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.exceptions import APIException

# app
from server.apps.common.exceptions import ServiceBaseException


class WrappedValidationException(ServiceBaseException):
    """Validation Exception."""

    status = 400
    scope = 'VALIDATION'
    code = 0
    message = 'One or more input fields have malformed values'

    def __init__(self, original_exc: APIException):
        """Init exception info."""
        field_errors = original_exc.get_full_details()
        non_field_errors = field_errors.pop('non_field_errors', [])
        self._kwargs = {'fieldErrors': field_errors, 'nonFieldErrors': non_field_errors}


class WrappedAPIException(ServiceBaseException):
    """Wrapped Exception for api."""

    status = 400
    scope = 'API_EXCEPTION'
    code = 0
    message = 'API EXCEPTION FROM BASE EXCEPTION'

    def __init__(self, original_exc: Union[APIException, ObjectDoesNotExist]):
        """Init exception info."""
        self.status = getattr(original_exc, 'status_code', status.HTTP_400_BAD_REQUEST)
        self.code = getattr(original_exc, 'default_code', 0)
        self.message = getattr(original_exc, 'default_detail', 0)

        self._kwargs = {'detail': getattr(original_exc, 'detail', None)}
