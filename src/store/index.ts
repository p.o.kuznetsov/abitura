import Vue from 'vue';
import Vuex from 'vuex';
import GraduationModule from './modules/graduations';
import ProgramModule from './modules/programs';
import ApplicationModule from './modules/application';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  modules: {
    GraduationModule,
    ProgramModule,
    ApplicationModule,
  },
});
