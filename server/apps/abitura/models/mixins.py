from datetime import datetime

from django.db import models

from server.apps.common.models.base import ORMCoreModel


class YearMixin(ORMCoreModel):
    year = models.SmallIntegerField()

    def save(self, *args, **kwargs) -> None:
        if not self.year:
            self.year = datetime.now().year
        return super().save(*args, **kwargs)

    def __str__(self):
        return f'id={self.id}, year={self.year}'

    class Meta:
        abstract = True


class CodeMixin(ORMCoreModel):
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=32)

    def __str__(self):
        return f'id={self.id}, name={self.name}, code={self.code}'

    class Meta:
        abstract = True
