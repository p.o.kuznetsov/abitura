from django.db.models import F, Window, Value, CharField
from django.db.models.aggregates import Sum
from django.db.models.functions import Rank, Concat
from rest_framework import viewsets

from server.apps.abitura import models
from server.apps.api.v1 import serializers, filters


class GraduationViewSet(viewsets.ModelViewSet):
    queryset = models.Graduation.objects.all()
    serializer_class = serializers.GraduationSerializer


class EducationProgramViewSet(viewsets.ModelViewSet):
    queryset = models.EducationProgram.objects.all()
    serializer_class = serializers.EducationProgramSerializer


class EducationFormViewSet(viewsets.ModelViewSet):
    queryset = models.EducationForm.objects.all()
    serializer_class = serializers.EducationFormSerializer


class EducationProgramInstanceViewSet(viewsets.ModelViewSet):
    queryset = models.EducationProgramInstance.objects.select_related(
        'education_form',
        'education_program',
        'graduation',
    ).prefetch_related(
        'direction_relations__requirements__subject',
        'direction_relations__program_direction',
    )
    serializer_class = serializers.EducationProgramInstanceSerializer
    filterset_class = filters.EducationProgramInstanceFilter


separator = Value(' ', output_field=CharField())


class AbiturientRateViewSet(viewsets.ModelViewSet):
    queryset = models.Attempt.objects.all().prefetch_related(
        'exams',
        'exams__subject',
    ).annotate(
        sum=Sum('exams__value'),
        username=Concat(
            F('abiturient__last_name'),
            separator,
            F('abiturient__first_name'),
            separator,
            F('abiturient__middle_name'),
        ),
        rank=Window(
            expression=Rank(),
            order_by=(
                F('has_accepted').desc(),
                F('has_original_document').desc(),
                F('sum').desc(),
                F('username'),
                F('abiturient'),
            ),
        ),
    )
    serializer_class = serializers.AttemptSerializer
    filterset_class = filters.AttemptRateFilter
