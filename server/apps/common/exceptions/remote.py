# django
from rest_framework.response import Response

# app
from server.apps.common.exceptions import ServiceBaseException


class RemoteServiceError(ServiceBaseException):
    """Remote exception."""

    scope = 'REMOTE'
    code = 0
    message = 'Remote service error'

    def __init__(self, remote_error_json=None, status=503, **kwargs):
        """Init exception info."""
        self.status = status
        self._remote_error_json = remote_error_json
        self._kwargs = kwargs

    def get_response(self):
        """Return remote_error."""
        if self._remote_error_json is None:
            return super().get_response()

        return Response(self._remote_error_json, status=self.status)
