# Курсовой проект по дисциплине "Базы данных"
## Автоматизированная электронно-справочная система "Приёмная комиссия"

## Сборка проекта для локального запуска
1. `cp .env.template .env` - скопировать .env файл и выставить нужные значения переменных окружения
2. `docker-compose build` - сборка
3. `docker-compose up -d` - запуск проекта
4. `docker-compose run api python manage.py createsupersuer` - создание пользователя со всеми правами

## Сборка проекта для деплоя
1. `cp .env.template .env` - скопировать .env файл и выставить нужные значения переменных окружения
2. `docker-compose -f docker-compose.yml -f docker/docker-compose.prod.yml config > docker-compose.deploy.yml`
2. `docker-compose -f docker-compose deploy.yml build` - сборка
3. `docker-compose -f docker-compose deploy.yml up -d` - запуск проекта
4. `docker-compose -f docker-compose deploy.yml run api python manage.py init_abitura` - заполнить бд
5. `docker-compose -f docker-compose deploy.yml run api python manage.py createsupersuer` - создание пользователя со 
всеми правами

### Использование проекта
Админ панель <http://127.0.0.1:8082/admin>  
API <http://127.0.0.1:8082>  
Фронтенд <http://127.0.0.1:8080>
