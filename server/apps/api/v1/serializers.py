from rest_framework import serializers

from server.apps.abitura import models


class GraduationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Graduation
        fields = (
            'id',
            'code',
            'name',
        )


class EducationProgramSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.EducationProgram
        fields = (
            'id',
            'name',
            'code',
        )


class EducationFormSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.EducationForm
        fields = (
            'id',
            'name',
            'code',
        )


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Subject
        fields = ('code', 'name')


class RateSerializer(serializers.ModelSerializer):
    code = serializers.CharField(source='subject.code')
    name = serializers.CharField(source='subject.name')

    class Meta:
        model = models.SubjectScore
        fields = (
            'code',
            'name',
            'value',
        )


class AttemptSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    score = RateSerializer(source='exams', many=True)
    rank = serializers.IntegerField()

    class Meta:
        model = models.Attempt
        fields = (
            'username',
            'score',
            'has_accepted',
            'has_original_document',
            'rank',
        )


class ProgramDirectionRelationSerializer(serializers.HyperlinkedModelSerializer):
    code = serializers.CharField(source='program_direction.code')
    name = serializers.CharField(source='program_direction.name')
    required_subjects = RateSerializer(source='requirements', many=True)

    class Meta:
        model = models.ProgramDirectionRelation
        fields = (
            'year',
            'places_count',
            'code',
            'name',
            'required_subjects',
        )


class EducationProgramInstanceSerializer(serializers.HyperlinkedModelSerializer):
    education_form = EducationFormSerializer()
    education_program = EducationProgramSerializer()
    graduation = GraduationSerializer()
    directions = ProgramDirectionRelationSerializer(source='direction_relations', many=True)

    class Meta:
        model = models.EducationProgramInstance
        fields = (
            'id',
            'education_form',
            'education_program',
            'graduation',
            'duration_years',
            'directions',
        )
