import Vue from 'vue';
import Layout from '@/components/Layout/Layout.vue';
import Dashboard from '@/pages/Dashboard/Dashboard.vue';
import EducationProgram from '@/pages/EducationProgram/EducationProgram.vue';


import VueRouter, { RouteConfig } from 'vue-router';


Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: '/bacelours',
    children: [
      {
        path: 'magistracy',
        name: 'Magistracy',
        component: Dashboard,
      },
      {
        path: 'bacelours',
        name: 'Bacelours',
        component: Dashboard,
      },
      {
        path: 'bacelours/programs/:id',
        name: 'Programs',
        component: EducationProgram,
      },
      {
        path: 'speciality',
        name: 'Speciality',
        component: Dashboard,
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
