import { IEducationForm } from './../../interfaces';
import { Module, VuexModule, MutationAction } from 'vuex-module-decorators';
import { IGraduation } from '@/interfaces';
import { getEducationForms } from '@/api';


@Module({ name: 'GraduationModule' })
export default class GraduationModule extends VuexModule {
  public educationForms: IEducationForm[] = [
    {
      name: 'Очная',
      code: 'ochka',
    },
    {
      name: 'Заочная',
      code: 'zaochka',
    },
  ];
  public graduations: IGraduation[] = [
    {
      name: 'Бакалавриат',
      code: 'bacelour',
    },
    {
      name: 'Магистратура',
      code: 'magistratura',
    },
    {
      name: 'Аспирантура',
      code: 'aspirantura',
    },
    {
      name: 'Специалитет',
      code: 'speciality',
    },
  ];

  @MutationAction({ mutate: ['graduations'] })
  public async fetchGraduations() {
    const graduations = this.graduations;
    return { graduations };
  }

  @MutationAction({ mutate: ['educationForms'] })
  public async fetchEducationForms() {
    const educationForms = await getEducationForms();
    return { educationForms };
  }
}
