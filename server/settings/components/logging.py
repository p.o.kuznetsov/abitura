LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': (
                '%(asctime)s [%(process)d] [%(levelname)s] ' +
                'pathname=%(pathname)s lineno=%(lineno)s ' +
                'funcname=%(funcName)s %(message)s'
            ),
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
        'simple': {
            '()': 'colorlog.ColoredFormatter',
            'format': (
                '%(cyan)s%(asctime)s%(reset)s ' +
                '%(log_color)s%(levelname)-8s%(reset)s %(cyan)s%(name)s:' +
                '%(funcName)s:%(lineno)-3d%(reset)s %(log_color)s%(message)s%(reset)s'
            ),
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'console-verbose': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {'handlers': ['console'], 'propagate': True, 'level': 'INFO'},
        'security': {
            'handlers': ['console-verbose'],
            'level': 'ERROR',
            'propagate': False,
        },
    },
}
