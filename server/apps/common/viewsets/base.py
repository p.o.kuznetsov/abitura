# built-in
from typing import TYPE_CHECKING, Dict, Type, Any

# django
from django.db.models import QuerySet
from rest_framework import viewsets
from rest_framework import serializers


if TYPE_CHECKING:  # pragma: no cover
    _Base = viewsets.ModelViewSet
else:
    _Base = object

BaseSerializer = Type[serializers.BaseSerializer]
SerializerStore = Dict[str, BaseSerializer]


class ViewSetMixin(_Base):
    """Base viewset with extended serializer classes."""

    request_serializer_classes: SerializerStore = {}
    response_serializer_classes: SerializerStore = {}
    querysets: Dict[str, 'QuerySet[Any]'] = {}

    def get_queryset(self) -> 'QuerySet[Any]':
        """Return queryset by action or default."""
        custom_queryset = self.querysets.get(getattr(self, 'action', None))
        return custom_queryset or super().get_queryset()

    def get_serializer_class(self) -> BaseSerializer:
        """Returns request serializer by action name."""
        return self._get_concrete_serializer(self.request_serializer_classes)

    def get_response_serializer_class(self) -> BaseSerializer:
        """Returns response serializer by action name."""
        return self._get_concrete_serializer(self.response_serializer_classes)

    def _get_concrete_serializer(self, serializer_store: SerializerStore) -> BaseSerializer:
        """Returns response serializer by action name."""
        action = getattr(self, 'action', None)
        if action:
            serializer = serializer_store.get(action, None)

            if serializer is not None:
                return serializer

        # return default serializer
        if getattr(self, 'serializer_class', None) is None or self.serializer_class is None:
            raise AttributeError('Declare serializer class')
        return self.serializer_class
