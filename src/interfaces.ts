export interface IGraduation {
  code: string;
  name: string;
}

export interface Code {
  code: string;
  name: string;
}

export interface IProgram {
  code: string;
  name: string;
}

export interface IEducationForm {
  code: string;
  name: string;
}

export interface ISubject {
  code: string;
  name: string;
}

export interface IDirection {
  year: number;
  placesCount: number;
  code: string;
  name: string;
  requiredSubjects: ISubject[];
}

export interface IProgramInstance {
  id: number;
  educationForm: IEducationForm;
  educationProgram: IProgram;
  graduation: IGraduation;
  durationYears: number;
  subjects: string[];
  career: string[];
  directions: IDirection[];
}

export interface IAbiturientScore {
  code: string;
  name: string;
  value: number;
}

export interface IAbiturientRate {
  username: string;
  score: IAbiturientScore[];
  hasOriginalDocument: boolean;
  hasAccepted: boolean;
  rank: number;
}

export interface IRateParams {
  programInstanceId: number;
  year: number;
  direction: string;
}

export interface IProgramParams {
  graduation: string;
  educationForm: string;
}
