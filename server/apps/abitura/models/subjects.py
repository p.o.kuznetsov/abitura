from django.db import models

from server.apps.common.models.base import ORMCoreModel
from .mixins import CodeMixin


class Subject(CodeMixin):
    """Math, chemical."""


class SubjectScore(ORMCoreModel):
    """Предмет и балл за него."""

    class SubjectScoreType(models.TextChoices):
        """Session startup type."""

        EXAM = 'EXAM'
        MINIMAL_REQUIREMENT = 'MINIMAL_REQUIREMENT'

    type = models.CharField(
        choices=SubjectScoreType.choices,
        max_length=32,
        default=SubjectScoreType.EXAM,
    )
    subject = models.ForeignKey('Subject', related_name='rates', on_delete=models.CASCADE)
    value = models.PositiveSmallIntegerField()
    attempt = models.ForeignKey(
        'Attempt',
        on_delete=models.CASCADE,
        related_name='exams',
        null=True,
    )
    direction_relation = models.ForeignKey(
        'ProgramDirectionRelation',
        on_delete=models.CASCADE,
        related_name='requirements',
        null=True,
    )

    def save(self, *args, **kwargs):
        if self.type == self.SubjectScoreType.EXAM:
            if not self.attempt or self.direction_relation:
                raise ValueError()
        elif self.type == self.SubjectScoreType.MINIMAL_REQUIREMENT:
            if self.attempt or not self.direction_relation:
                raise ValueError()
        return super().save(*args, **kwargs)

    def __str__(self):
        return f'id={self.id}, subject={self.subject}, value={self.value}'
