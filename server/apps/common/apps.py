# django
from django.apps import AppConfig


class CommonConfig(AppConfig):
    """Класс приложения."""

    name = 'server.apps.common'
